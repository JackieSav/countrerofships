import styled from 'styled-components';
import InputBlock from './components/InputBlock/InputBlock';
import FieldOfShips from './components/FieldOfShips/FieldOfShips';
import { IFieldSqueres } from './types/interfaces';
import { useState } from 'react';
import ResultPanel from './components/ResultPanel/ResultPanel';
const MainWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`

function App() {

  const [field, setField] = useState<IFieldSqueres[]>([]);
  const [col, setCol] = useState<number>(0);
  const [ships, setShips] = useState<number>(0);

  const onFieldCreate = (sq: IFieldSqueres[] , col: number) => {
    setField([]);
    setField(prev => [...prev, ...sq]);
    setCol(col);
  }

  const onSquereMark = (id: number) => {
    setField(prev => [...prev.map((el) => (
      el.id === id ? {...el, color: 'black'} : {...el}
    ))])
  }

  const onCountOfShips = () => {
    let matrix: IFieldSqueres[] = JSON.parse(JSON.stringify(field));
    console.log('field1', field);
    let ships: number = 0;
    for (let i = 0; i < matrix.length; i += 1) {
      console.log(i);
      if (matrix[i].color === 'black') {
        matrix[i].color = 'white';
        if(matrix[i + 1] && matrix[i + 1].color === 'black') {
          matrix[i + 1].color = 'white';
          if(matrix[i + 2] && matrix[i + 2].color === 'black') {
            matrix[i + 2].color = 'white';
            if(matrix[i + 3] && matrix[i + 3].color === 'black') {
              matrix[i + 3].color = 'white';
            }
          }
        }
        if(matrix[i + col] && matrix[i + col].color === 'black') {
          matrix[i + col].color = 'white';
          if(matrix[i + col * 2] && matrix[i + (col * 2)].color === 'black') {
            matrix[i + col * 2].color = 'white';
            if(matrix[i + col * 2] && matrix[i + (col * 2)].color === 'black') {
              matrix[i + col * 2].color = 'white';
            }
          }
        }
        ships += 1;
      }
    }
    setShips(ships);
    console.log('field2',field);
    
  }

  return (
    <MainWrapper>
      <InputBlock onFieldCreate={onFieldCreate}></InputBlock>
      {field.length > 1 && 
        <FieldOfShips field={field} col={col} onSquereMark={onSquereMark}/>
      }
      {field.length > 1 && 
        <ResultPanel onCountOfShips={onCountOfShips} ships={ships}/>
      }
    </MainWrapper>
  );
}

export default App;

import styled from 'styled-components';
import { IResultPanelProps } from '../../types/interfaces';

const ResultBlock = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  width: 600px;
  height: 100px;
  background-color: #4051B5;
  border-radius: 10px 10px 0px 0px;
  margin-top: 10px;
`

const ResultBtn = styled.button`
  width: 100px;
  height: 52px;
  font-size: 10px;
  margin-left: 10px;
  margin-right: 10px;
`
const ResultWindow = styled.div`
  width: 100px;
  height: 50px;
  background-color: white;
  margin-left: 10px;
  margin-rigth: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`


const ResultPanel:React.FC<IResultPanelProps> = ({onCountOfShips, ships}) => {
  return (
    <ResultBlock>
      <ResultBtn  type="button" onClick={onCountOfShips}>Посчитать корабли</ResultBtn>
      <ResultWindow>
        {ships > 0 && 
          <h1>{ships}</h1>
        }
      </ResultWindow>
    </ResultBlock>
  );
};

export default ResultPanel;
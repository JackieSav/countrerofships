import React from 'react';
import styled from 'styled-components';
import { TFieldBlock, TOneShipSquere } from '../../types/types';
import { IFieldOfShipsProps } from '../../types/interfaces';

const FieldBlock = styled.div<TFieldBlock>`
  width: ${({ width }) => width};
  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 10px;
`

const OneShipSquere = styled.div<TOneShipSquere>`
  width: ${({ width }) => width};
  height: ${({ height }) => height};
  border: 1px solid black;
  background-color:  ${({ color }) => color };
`

const FieldOfShips:React.FC<IFieldOfShipsProps> = ({field, col, onSquereMark}) => {
  
  return (
    <FieldBlock width={`${400 + (col * 2)}px`}>
      {field.map((el) => <OneShipSquere 
        key={el.id} 
        height={`${400 / col}px`} 
        width={`${400 / col}px`}
        color={el.color}
        onClick={() => onSquereMark(el.id)}
        />
      )}
    </FieldBlock>
  );
};

export default FieldOfShips;
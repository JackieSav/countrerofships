import styled from "styled-components";
import { useState } from "react";
import { ICreateFieldProps, IFieldSqueres } from "../../types/interfaces";

const MainInputForm = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  width: 600px;
  height: 100px;
  background-color: #4051B5;
  border-radius: 0px 0px 10px 10px;
`
const TableIntput = styled.input`
  width: 60px;
  height: 25px;
  font-size: 10px;
  margin-left: 10px;
  margin-rigth: 10px;
`

const CreateBtn = styled.button`
  width: 80px;
  height: 32px;
  font-size: 10px;
  margin-left: 10px;
  margin-rigth: 10px;
`

const InputBlock:React.FC<ICreateFieldProps> = ({onFieldCreate}) => {
  const [column, setColumn] = useState('');
  const [string, setString] = useState('');



  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (Number(column) > 0 && Number(string) > 0) {
      const count = Number(column) * Number(string);
      let squeres: IFieldSqueres[] = [];
      for (let i = 1; i <= count; i += 1) {
        const squere = {
          id: i,
          color: 'white'
        }
        squeres.push(squere)
      }
      onFieldCreate(squeres, Number(column))
      setColumn('');
      setString('')
      squeres = [];
    }
  }

  return (
    <MainInputForm onSubmit={handleSubmit}>
      <TableIntput type='number' placeholder="столбцы" onChange={(e) => setColumn(e.currentTarget.value)} value={column} />
      <TableIntput type='number' placeholder="строки" onChange={(e) => setString(e.currentTarget.value)} value={string} />
      <CreateBtn type="submit">Создать поле</CreateBtn>
    </MainInputForm>
  );
};

export default InputBlock;
export interface ICreateFieldProps {
  onFieldCreate: (squeres: IFieldSqueres[], str: number) => void;
}

export interface IFieldSqueres {
  id: number
  color: string
}

export interface IFieldOfShipsProps {
  field: IFieldSqueres[]
  col: number
  onSquereMark: (id: number) => void;
}

export interface IResultPanelProps {
  onCountOfShips: () => void
  ships: number
}
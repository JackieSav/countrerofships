export type TOneShipSquere = {
  width: string
  height: string
  color: string
}

export type TFieldBlock = {
  width: string
}
